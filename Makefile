# Build a Summation C program

CC     = gcc
CFLAGS = -g -Wall

TARGET = Summation 

all: $(TARGET)

summation: summation.c
	$(CC) $(CFLAGS) -o $(TARGET) summation.c

clean:
	rm $(TARGET)

